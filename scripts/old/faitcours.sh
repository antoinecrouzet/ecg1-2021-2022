cd ~/Cloud/Enseignement/ECG/Cours

if [ -z $2 ];
then
 make pdf CHAP=$1
else
 make pdf CHAP=$1 VER=oui
fi

open PDF/$1
