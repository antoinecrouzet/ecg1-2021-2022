# On récupère le chapitre
REPS = $(filter-out $@,$(MAKECMDGOALS))
REP=$(REPS)
CHAP=$(REP)

TITRE = $(shell head -n 1 "tex/Chap$(CHAP)/Cours.tex" | cut -d "{" -f 2 | cut -d "}" -f 1 | sed "s/\\\'//g" )
APP=/Applications/Atom.app

define compile_tout
for i in $$( seq 1 $(NB)); \
do \
  if [ $$i -le 9 ]; then  num=0$$i; else num=$$i; fi;\
  eval ${MAKE} pdf $$num; \
done
endef 

ifeq ($(strip $(VER)),)
  VERSION="\\\versionfalse"
else
  VERSION="\\\versiontrue"
endif

pdf: REP = $(filter-out pdf,$(REPS))
edit: REP = $(filter-out edit,$(REPS))

pdf: titrechap chapprof chapeleve chappubli clean
edit: titrechap edition 
complet: courscomplet clean
	
	
editexo:
	@echo $(REPS)
	@echo $(CHAP)
	@$(if $(strip $(EXO)),eval ${MAKE} editionexo,echo "Manque arg NB") 
	@eval ${MAKE} clean

toutpdf:
	@$(if $(strip $(NB)),${call compile_tout},echo "Manque arg NB") 
#	@${call compile_tout}

titre:
	@./scripts/titre
	
polyexo:
	@./scripts/polyexo
	@eval ${MAKE} clean

polycours:
	@./scripts/polycours
	@eval ${MAKE} clean
	
titrechap:
	@printf "Traitement du chapitre \033[31;01m%s\033[00m : \033[31;01m%s\033[00m\n" $(REP) "$(TITRE)"

edition:
	@echo "\\\documentclass[maths,noir]{antoine2021}" > encours.nouveau.tex
	@echo "" >> encours.nouveau.tex
	@echo "\\\newif\\\ifprof\\\proftrue" >> encours.nouveau.tex
	@echo "\\\newif\\\ifversion" >> encours.nouveau.tex
	@echo $(VERSION) >> encours.nouveau.tex
	@echo "\\\newif\\\ifcorrection\\\correctiontrue" >> encours.nouveau.tex
	@echo "\\\titreprepa{CPGE ECG}" >> encours.nouveau.tex
	@echo "\\\titredulivre{Maths. approfondies}" >> encours.nouveau.tex
	@echo "\\\datedulivre{2021--2022}" >> encours.nouveau.tex
	@echo "" >> encours.nouveau.tex
	@echo "\\\makeatletter" >>  encours.nouveau.tex
	@echo "\\\def\\\input@path{{tex/graphe/$(CHAP)}{tex/td/$(CHAP)}{tex/script/$(CHAP)}{tex/pic/$(CHAP)}}" >> encours.nouveau.tex
	@echo "\\\graphicspath{{tex/pic/$(CHAP)}}" >> encours.nouveau.tex
#	@echo "\\\lstset{inputpath=tex/script/$(CHAP)}" >> encours.nouveau.tex
	@echo "\\\makeatother" >> encours.nouveau.tex
	@echo "" >> encours.nouveau.tex
	@echo "\\\begin{document}" >> encours.nouveau.tex
	@echo "\\\commencecours{$(CHAP)}" >> encours.nouveau.tex
	@echo "\\\end{document}" >> encours.nouveau.tex
	@printf "Edition du chapitre %s...\n" $(CHAP)
	@open -a $(APP) ./encours.nouveau.tex
	@while pgrep -f $(APP) >/dev/null ; do sleep 1.0 ; done
	@printf "Edition terminée.\n"
	@eval ${MAKE} clean

chapprof:
	@printf "[  ] Compilation du chapitre %s - prof" $(REP)
	@echo "\\\documentclass[maths]{antoine2021}" > encours.nouveau.tex
	@echo "" >> encours.nouveau.tex
	@echo "\\\newif\\\ifprof" >> encours.nouveau.tex
	@echo "\\\proftrue" >> encours.nouveau.tex
	@echo "\\\newif\\\ifversion\\\versionfalse" >> encours.nouveau.tex
	@echo "\\\newif\\\ifcorrection\\\correctiontrue" >> encours.nouveau.tex
	@echo "\\\titreprepa{CPGE ECG 1}" >> encours.nouveau.tex
	@echo "\\\titredulivre{Maths. approfondies}" >> encours.nouveau.tex
	@echo "\\\datedulivre{2021--2022}" >> encours.nouveau.tex
	@echo "%\\\fancyfoot[L]{}" >> encours.nouveau.tex
	@echo "%\\\fancyfoot[R]{}" >> encours.nouveau.tex
	@echo "" >> encours.nouveau.tex
	@echo "\\\makeatletter" >>  encours.nouveau.tex
	@echo "\\\def\\\input@path{{tex/graphe/$(CHAP)}{tex/td/$(CHAP)}{tex/script/$(CHAP)}{tex/pic/$(CHAP)}}" >> encours.nouveau.tex
	@echo "\\\graphicspath{{tex/pic/$(CHAP)}}" >> encours.nouveau.tex
#	@echo "\\\lstset{inputpath=tex/script/$(CHAP)}" >> encours.nouveau.tex
	@echo "\\\makeatother" >> encours.nouveau.tex
	@echo "" >> encours.nouveau.tex
	@echo "\\\begin{document}" >> encours.nouveau.tex
	@echo "\\\commencecours{$(CHAP)}" >> encours.nouveau.tex
	@echo "\\\end{document}" >> encours.nouveau.tex
#	@latexmk -quiet -xelatex encours.nouveau.tex > /dev/null 2>&1
	@mkdir -p "PDF/$(REP)"
	@$(RM) PDF/$(REP)/*prof.pdf
	@./scripts/compile
	@./scripts/renomme $(REP) prof "$(TITRE)"
	@printf "\r[\033[31;01mOK\033[00m] Compilation du chapitre %s - prof\n" $(REP)

chapeleve:
	@printf "[  ] Compilation du chapitre %s - eleve" $(REP)
	@echo "\\\documentclass[maths,noir]{antoine2021}" > encours.nouveau.tex
	@echo "" >> encours.nouveau.tex
	@echo "\\\newif\\\ifprof\\\proffalse" >> encours.nouveau.tex
	@echo "\\\newif\\\ifversion\\\versionfalse" >> encours.nouveau.tex
	@echo "\\\newif\\\ifcorrection\\\correctionfalse" >> encours.nouveau.tex
	@echo "\\\titreprepa{Lycée Carnot}" >> encours.nouveau.tex
	@echo "\\\titredulivre{Maths. approfondies}" >> encours.nouveau.tex
	@echo "\\\datedulivre{2021--2022}" >> encours.nouveau.tex
	@echo "%\\\fancyfoot[L]{}" >> encours.nouveau.tex
	@echo "%\\\fancyfoot[R]{}" >> encours.nouveau.tex
	@echo "" >> encours.nouveau.tex
	@echo "\\\makeatletter" >>  encours.nouveau.tex
	@echo "\\\def\\\input@path{{tex/graphe/$(CHAP)}{tex/td/$(CHAP)}{tex/script/$(CHAP)}{tex/pic/$(CHAP)}}" >> encours.nouveau.tex
	@echo "\\\graphicspath{{tex/pic/$(CHAP)}}" >> encours.nouveau.tex
#	@echo "\\\lstset{inputpath=tex/script/$(CHAP)}" >> encours.nouveau.tex
	@echo "\\\makeatother" >> encours.nouveau.tex
	@echo "" >> encours.nouveau.tex
	@echo "\\\begin{document}" >> encours.nouveau.tex
	@echo "\\\setcounter{chapter}{" >> encours.nouveau.tex
	@echo $(shell echo $(CHAP)\-1 | bc) >> encours.nouveau.tex
	@echo "}" >> encours.nouveau.tex
	@echo "\\\begin{cours}" >> encours.nouveau.tex
	@echo "\\\include{tex/Chap$(CHAP)/Cours}" >> encours.nouveau.tex
	@echo "\\\ifodd\\\thepage\\\else \\\hbox{}\\\fi" >> encours.nouveau.tex
	@echo "\\\end{cours}" >> encours.nouveau.tex
	@echo "\\\include{tex/Chap$(CHAP)/Exo}" >> encours.nouveau.tex	
#	@echo "\\\commencecours{$(CHAP)}" >> encours.nouveau.tex
	@echo "\\\end{document}" >> encours.nouveau.tex
#	@latexmk -quiet -xelatex encours.nouveau.tex > /dev/null 2>&1
	@mkdir -p "PDF/$(REP)"
	@$(RM) PDF/$(REP)/*eleve.pdf
	@./scripts/compile
	@./scripts/renomme $(REP) eleve "$(TITRE)"
	@printf "\r[\033[31;01mOK\033[00m] Compilation du chapitre %s - eleve\n" $(REP)

chappubli:
	@printf "[  ] Compilation du chapitre %s - publi" $(REP)
	@echo "\\\documentclass[maths]{antoine2021}" > encours.nouveau.tex
	@echo "" >> encours.nouveau.tex
	@echo "\\\newif\\\ifprof\\\proftrue" >> encours.nouveau.tex
	@echo "\\\newif\\\ifversion\\\versiontrue" >> encours.nouveau.tex
	@echo "\\\newif\\\ifcorrection\\\correctionfalse" >> encours.nouveau.tex
	@echo "\\\titreprepa{CPGE ECG 1}" >> encours.nouveau.tex
	@echo "\\\titredulivre{Maths. approfondies}" >> encours.nouveau.tex
	@echo "\\\datedulivre{2021--2022}" >> encours.nouveau.tex
	@echo "%\\\fancyfoot[L]{\\\href{mailto:antoine.crouzet@normalesup.org}{A. Crouzet}}" >> encours.nouveau.tex
	@echo "%\\\fancyfoot[R]{\\\doclicenseIcon}" >> encours.nouveau.tex
	@echo "" >> encours.nouveau.tex
	@echo "\\\makeatletter" >>  encours.nouveau.tex
	@echo "\\\def\\\input@path{{tex/graphe/$(CHAP)}{tex/td/$(CHAP)}{tex/script/$(CHAP)}{tex/pic/$(CHAP)}}" >> encours.nouveau.tex
	@echo "\\\graphicspath{{tex/pic/$(CHAP)}}" >> encours.nouveau.tex
#	@echo "\\\lstset{inputpath=tex/script/$(CHAP)}" >> encours.nouveau.tex
	@echo "\\\makeatother" >> encours.nouveau.tex
	@echo "" >> encours.nouveau.tex
	@echo "\\\begin{document}" >> encours.nouveau.tex
	@echo "\\\commencecours{$(CHAP)}" >> encours.nouveau.tex
	@echo "\\\end{document}" >> encours.nouveau.tex
#	@latexmk -quiet -xelatex encours.nouveau.tex > /dev/null 2>&1
	@mkdir -p "PDF/$(REP)"
	@$(RM) PDF/$(REP)/*publi.pdf
	@./scripts/compile 
	@./scripts/renomme $(REP) publi "$(TITRE)"
	@printf "\r[\033[31;01mOK\033[00m] Compilation du chapitre %s - publi\n" $(REP)

courscomplet:
	@cp tex/cours_complet.tex encours.complet.tex
	@./scripts/compilecomplet
	@mv encours.complet.pdf "PDF/Cours complet.pdf"

clean:
	@printf "Nettoyage.\n"
	@${RM} encours.* 
	@${RM} -rf _minted-encours.nouveau
	@find . -name "*.aux" -exec rm {} \;
	
editionexo:
	@echo "\\\documentclass[maths,noir]{antoine2021}" > encours.nouveau.tex
	@echo "" >> encours.nouveau.tex
	@echo "\\\newif\\\ifprof\\\proftrue" >> encours.nouveau.tex
	@echo "\\\newif\\\ifversion" >> encours.nouveau.tex
	@echo $(VERSION) >> encours.nouveau.tex
	@echo "\\\newif\\\ifcorrection\\\correctiontrue" >> encours.nouveau.tex
	@echo "\\\titreprepa{CPGE ECG}" >> encours.nouveau.tex
	@echo "\\\titredulivre{Maths. approfondies}" >> encours.nouveau.tex
	@echo "\\\datedulivre{2021--2022}" >> encours.nouveau.tex
	@echo "" >> encours.nouveau.tex
	@echo "\\\makeatletter" >>  encours.nouveau.tex
	@echo "\\\def\\\input@path{{tex/graphe/$(CHAP)}{tex/td/$(CHAP)}{tex/script/$(CHAP)}{tex/pic/$(CHAP)}}" >> encours.nouveau.tex
	@echo "\\\graphicspath{{tex/pic/$(CHAP)}}" >> encours.nouveau.tex
#	@echo "\\\lstset{inputpath=tex/script/$(CHAP)}" >> encours.nouveau.tex
	@echo "\\\makeatother" >> encours.nouveau.tex
	@echo "" >> encours.nouveau.tex
	@echo "\\\begin{document}" >> encours.nouveau.tex
	@echo "\\exotd{$(EXO)}}" >> encours.nouveau.tex
	@echo "\\\end{document}" >> encours.nouveau.tex
	@printf "Edition du chapitre %s - exercice ...\n" $(CHAP) $(EXO)
	@open -a $(APP) ./encours.nouveau.tex
	@while pgrep -f $(APP) >/dev/null ; do sleep 1.0 ; done
	@printf "Edition terminée.\n"
	@eval ${MAKE} clean

# Pour les autres arguments
%:      # thanks to chakrit
	@:    # thanks to William Pursell
