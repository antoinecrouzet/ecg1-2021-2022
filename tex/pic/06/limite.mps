%!PS
%%BoundingBox: -24 -15 298 86 
%%HiResBoundingBox: -23.6607 -14.42323 297.88774 85.28935 
%%Creator: MetaPost 2.00
%%CreationDate: 2020.11.04:1239
%%Pages: 1
%*Font: cmmi5 4.98132 4.98132 22:8000000000000002
%*Font: cmr5 4.98132 4.98132 2b:8
%*Font: cmsy5 4.98132 4.98132 00:8
%*Font: cmmi9 8.96637 8.96637 6e:8
%*Font: cmr6 5.97758 5.97758 30:8
%%BeginProlog
%%EndProlog
%%Page: 1 1
 0.9 0.9 0.9 setrgbcolor
newpath 113.3858 48.18887 moveto
297.63774 48.18887 lineto
297.63774 65.19693 lineto
113.3858 65.19693 lineto
 closepath fill
 0 0 0 setrgbcolor 0 0.5 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath -14.17323 0 moveto
297.63774 0 lineto stroke
newpath 293.941 -1.53128 moveto
297.63774 0 lineto
293.941 1.53128 lineto
 closepath
gsave fill grestore stroke
 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 0 -14.17323 moveto
0 85.03935 lineto stroke
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath 1.53061 81.34421 moveto
0 85.03935 lineto
-1.53061 81.34421 lineto
 closepath
gsave fill grestore stroke
 0 0 1 setrgbcolor [3 3 ] 0 setdash
newpath -5.6692 65.19693 moveto
297.63774 65.19693 lineto stroke
 0 0 0 setrgbcolor
-23.384 63.88243 moveto
(`) cmmi5 4.98132 fshow
-18.7793 63.88243 moveto
(+) cmr5 4.98132 fshow
-12.0299 63.88243 moveto
(") cmmi5 4.98132 fshow
 0 0 1 setrgbcolor
newpath -5.6692 48.18887 moveto
297.63774 48.18887 lineto stroke
 0 0 0 setrgbcolor
-23.6607 46.87437 moveto
(`) cmmi5 4.98132 fshow
-19.056 46.87437 moveto
(\000) cmsy5 4.98132 fshow
-12.0298 46.87437 moveto
(") cmmi5 4.98132 fshow
 1 0 0 setrgbcolor [] 0 setdash
newpath -5.6692 56.6929 moveto
297.63774 56.6929 lineto stroke
-11.64421 54.9633 moveto
(`) cmmi5 4.98132 fshow
 0 0 0 setrgbcolor 0 2 dtransform truncate idtransform setlinewidth pop
newpath 28.34645 28.34645 moveto 0 0 rlineto stroke
newpath 56.6929 70.86613 moveto 0 0 rlineto stroke
newpath 85.03935 47.24423 moveto 0 0 rlineto stroke
newpath 113.3858 63.77951 moveto 0 0 rlineto stroke
newpath 141.73225 51.0237 moveto 0 0 rlineto stroke
newpath 170.0787 61.41745 moveto 0 0 rlineto stroke
newpath 198.42516 52.64354 moveto 0 0 rlineto stroke
newpath 226.7716 60.2362 moveto 0 0 rlineto stroke
newpath 255.11806 53.5432 moveto 0 0 rlineto stroke
newpath 283.46451 59.52773 moveto 0 0 rlineto stroke
 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 113.3858 65.19693 moveto
113.3858 0 lineto stroke
108.53079 -6.8605 moveto
(n) cmmi9 8.96637 fshow
114.0898 -7.8567 moveto
(0) cmr6 5.97758 fshow
showpage
%%EOF
