%!PS
%%BoundingBox: -15 -8 298 157 
%%HiResBoundingBox: -14.42323 -7.8567 297.88774 156.15556 
%%Creator: MetaPost 2.00
%%CreationDate: 2020.11.04:1245
%%Pages: 1
%*Font: cmmi5 4.98132 4.98132 41:8
%*Font: cmmi9 8.96637 8.96637 6e:8
%*Font: cmr6 5.97758 5.97758 30:8
%%BeginProlog
%%EndProlog
%%Page: 1 1
 0.9 0.9 0.9 setrgbcolor
newpath 113.3858 56.69293 moveto
297.63774 56.69293 lineto
297.63774 148.81895 lineto
113.3858 148.81895 lineto
 closepath fill
 0 0 0 setrgbcolor 0 0.5 dtransform truncate idtransform setlinewidth pop
 [] 0 setdash 1 setlinecap 1 setlinejoin 10 setmiterlimit
newpath -14.17323 0 moveto
297.63774 0 lineto stroke
newpath 293.941 -1.53128 moveto
297.63774 0 lineto
293.941 1.53128 lineto
 closepath
gsave fill grestore stroke
 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
newpath 0 -7.08662 moveto
0 155.90556 lineto stroke
 0 0.5 dtransform truncate idtransform setlinewidth pop
newpath 1.53046 152.2108 moveto
0 155.90556 lineto
-1.53046 152.2108 lineto
 closepath
gsave fill grestore stroke
 0 0 1 setrgbcolor [3 3 ] 0 setdash
newpath -5.6692 56.69293 moveto
297.63774 56.69293 lineto stroke
 0 0 0 setrgbcolor
-13.8305 54.99098 moveto
(A) cmmi5 4.98132 fshow
 0 2 dtransform truncate idtransform setlinewidth pop [] 0 setdash
newpath 28.34645 0 moveto 0 0 rlineto stroke
newpath 56.6929 35.43309 moveto 0 0 rlineto stroke
newpath 85.03935 37.79536 moveto 0 0 rlineto stroke
newpath 113.3858 60.23624 moveto 0 0 rlineto stroke
newpath 141.73225 68.03156 moveto 0 0 rlineto stroke
newpath 170.0787 87.40167 moveto 0 0 rlineto stroke
newpath 198.42516 97.18794 moveto 0 0 rlineto stroke
newpath 226.7716 115.15752 moveto 0 0 rlineto stroke
newpath 255.11806 125.98425 moveto 0 0 rlineto stroke
newpath 283.46451 143.14973 moveto 0 0 rlineto stroke
 0.5 0 dtransform exch truncate exch idtransform pop setlinewidth
 [3 3 ] 0 setdash
newpath 113.3858 60.23624 moveto
113.3858 0 lineto stroke
108.53079 -6.8605 moveto
(n) cmmi9 8.96637 fshow
114.0898 -7.8567 moveto
(0) cmr6 5.97758 fshow
showpage
%%EOF
