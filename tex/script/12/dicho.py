''' On suppose que f est définie précédemment
dicho prend trois arguments x, y, eps :
- [x,y] représente l'intervalle de recherche de départ
- eps la précision voulue'''

def f(x): return x**2-2   # un exemple avec $f:x \mapsto x^2-2$

def dicho(x,y,eps):
    a=x
    b=y
    while (b-a > eps):
        m=(a+b)/2
        if f(a)*f(m) <= 0:
            b=m
        else:
            a=m
    return [a,b]

dicho(0,2,0.001) # renvoie [1.4140625, 1.4150390625]
