import matplotlib.pyplot as plt
import pylab
x=[ i for i in range(2,13)]
h = [ i/36 for i in range(1,7)] + [(6-i)/36 for i in range(1, 6)] 
BarName = [ '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']
width = 0.1
plt.bar(x, h, width, color='red' )
plt.xlim(1,13)
plt.ylim(0, 0.2)
#plt.grid()
pylab.xticks(x, BarName)
plt.savefig('exo_deuxdes_baton.png')
plt.show()