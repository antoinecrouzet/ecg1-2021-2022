import matplotlib.pyplot as plt
import pylab
import numpy as np
from math import floor
#import scipy.stats as st


## Fonction
def U(x):
    if x<2:
        return 0
    if x <8:
        return (x-1)/36
    else:
        return (13-x)/36

def V(x):
    n=floor(x)
    return sum([U(i) for i in range(2,n+1)])

def sommeprob(n):
    if n<2:
        return 0
    if n<8:
        return int((n-1)*(n)/2)
    return int(21 + sum([13-i for i in range(8, n+1)]))

N=13*12*11*5*7
# On génére des points pour la fonction
x = [ 13*i/N for i in range(N)]
y = [V(i) for i in x]

for i in range(2,13):
    y[ x.index(float(i))] = np.nan

plt.plot(x, y, 'r')
plt.plot([i for i in range(2,13)], [V(i) for i in range(2,13)], 'ro', linewidth=.5, markersize=3)
axes = plt.gca()
axes.xaxis.set_ticks(range(0,14))
axes.yaxis.set_ticks([V(i) for i in  range(1,13)])
axes.yaxis.set_ticklabels(['0']+[str(sommeprob(i))+'/36' for i in range(2,12)]+[1], fontsize = 7)

#cdf = np.cumsum(h)
#BarName = [ '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']
#pylab.xticks(x, range(1,13))
#pylab.yticks(h,h)
#plt.step(x, cdf, "r", label="F_X")
#plt.xlim(1,13)
#plt.ylim(0, 1.01)
#print(cdf)

#loi = st.binom(10,0.3)
#print(loi)



plt.savefig('exo_deuxdes_fct_rep.png')
plt.show()