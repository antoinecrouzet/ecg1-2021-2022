%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{20}{2}[D'autres intégrales, IPP et changement de variables]
Justifier l'existence de ces intégrales, puis calculer leur valeur.
\begin{remarque}
On ne fait des intégrations par parties que sur des segments.
\end{remarque}
\everymath{\ds}
\begin{tasks}(2)
 \task $\int_{-\infty}^{+\infty} \frac{\D x}{1+x^{2}}$,
 \task $\int_{1}^{+\infty} \frac{\D x}{(1+x)(2+x)}$. \\~\\On pourra remarquer que  $1=(x+2)-(x+1)$ pour tout $x \in \R$.
 \task $\int_{1}^{+\infty} \ln \left(1+\frac{\alpha}{x^{2}}\right) \D x$, où $\alpha \in \R>$.
 \task $\int_{1}^{+\infty} \frac{\arctan(x)}{x^{2}} \D x$.
 \task $\int_{-\infty}^{+\infty} \frac{\D x}{\eu{x}+\eu{-x}}$. On posera $x=\ln (u)$.
 \task $\int_{0}^{+\infty} \frac{x \ln (x)}{\left(1+x^{2}\right)^{2}} \D x$. On posera $x=\frac{1}{t}$.
 \task $I_n=\int_{0}^{+\infty} \frac{\D x}{\left(1+x^{2}\right)^{n}}$, où $n \in \N*$.\\~\\
 On remarquera que $1=\left(1+x^{2}\right)-x^{2}$ pour tout $x \in \R$, puis exprimer $I_{n+1}$ en fonction de $I_{n}$.
\end{tasks}
\everymath{}
\end{exoApp}

\begin{correction}
{\allowdisplaybreaks
\begin{enumerate}
  \item $x\donne \frac{1}{1+x^2}$ est continue sur $\R$.   La fonction étant paire, il suffit d'étudier une des impropreté, en $+\infty$ ou $-\infty$. Or, on a \[ \frac{1}{1+x^2} \eq{+\infty} \frac{1}{x^2}.\]
Puisque l'intégrale $\ds{\int_1^{+\infty} \frac{\D x}{x^2}}$ converge, par comparaison de fonctions positives, l'intégrale $\ds{\int_1^{+\infty} \frac{\D x}{1+x^2}}$ converge, et notre intégrale converge par parité. Enfin, pour tout $a>0$ :
\begin{align*}
  \int_{-a}^a \frac{\D x}{1+x^2} &= \left[ \arctan(x)\right]_{-a}^a \\
  &= \arctan(a)-\arctan(-a) \tendversen{a\to +\infty} \frac{\pi}{2}-\left(-\frac{\pi}{2}\right) = \pi.
\end{align*}
Ainsi, \[ \boxed{\int_{-\infty}^{+\infty} \frac{\D x}{1+x^2} = \pi.}\]
\item $x\donne \frac{1}{(1+x)(2+x)}$ est continue sur $\interfo{1 +\infty}$. En $+\infty$, \[ \frac{1}{(1+x)(2+x)} \eq{+\infty} \frac{1}{x^2}. \]
Puisque $\ds{\int_1^{+\infty} \frac{\D x}{x^2}}$ converge (Riemann, $2>1$), par comparaison de fonctions positives, $\ds{\int_1^{+\infty} \frac{\D x}{(1+x)(2+x)}}$ converge également.\\ En utilisant l'indication, pour tout $a>1$ :
\begin{align*}
  \int_1^a \frac{\D x}{(1+x)(2+x)} &= \int_1^a \frac{(x+2)-(x+1)}{(1+x)(2+x)}\D x\\
  &= \int_1^a\left( \frac{1}{1+x}-\frac{1}{2+x}\right) \D x\\
  &= \int_1^a \left[ \ln(|1+x|)-\ln(|2+x|) \right]_1^a\\
  &= \ln\left(\frac{1+a}{2+a}\right) - \ln\left(\frac{2}{3}\right) \tendversen{a\to +\infty} -\ln\left(\frac{2}{3}\right)
\end{align*}
puisque $\frac{1+a}{2+a}\eq{+\infty} \frac{a}{a}=1 \tendversen{a\to +\infty} 1$, et par continuité de la fonction $\ln$. Ainsi, \[ \boxed{\int_1^{+\infty} \frac{\D x}{(1+x)(2+x)} = \ln\left(\frac{3}{2}\right).}\]
\item La fonction $x\donne \ln\left(1+\frac{\alpha}{x^2}\right)$ est continue sur $\interfo{1 +\infty}$. En $+\infty$, \[ \ln\left(1+\frac{\alpha}{x^2}\right) \eq{+\infty} \frac{\alpha}{x^2}. \]
Puisque $\ds{\int_1^{+\infty} \frac{\alpha}{x^2}\D x}$ converge (Riemann avec $2>1$), par comparaison de fonctions positives (puisque $\alpha >0$), l'intégrale $\ds{\int_1^{+\infty} \ln\left(1+\frac{\alpha}{x^2}\right)\D x}$ converge.

On va procéder par intégration par parties. \textbf{Attention} : on ne fait une IPP que sur un segment. Soit $b>1$. On pose $u:x\donne \ln \left(1+\frac{\alpha}{x^2}\right)$, $v':x\donne 1$. Ainsi, $u':x\donne \frac{-2\frac{\alpha}{x^3}}{1+\frac{\alpha}{x^2}}=-\frac{2\alpha}{x^3+\alpha x}$ et $v:x\donne x$.

$u$ et $v$ sont de classe $\CC^1$ sur $\interff{1 b}$. Par intégration par parties :
\begin{align*}
  \int_1^b \ln\left(1+\frac{\alpha}{x^2}\right)\D x &= \left[ x\ln\left(1+\frac{\alpha}{x^2}\right) \right]_1^b - \int_1^b x\frac{-2\alpha}{x^3+\alpha x} \D x \\
  &= b\ln\left(1+\frac{\alpha}{b^2}\right) - \ln\left(1+\alpha\right) +2\alpha \int_1^b \frac{1}{x^2+\alpha}\D x\\
  &= b\ln\left(1+\frac{\alpha}{b^2}\right) - \ln\left(1+\alpha\right) +2\alpha\int_1^b \frac{1}{\alpha} \frac{1}{1+\left(\frac{x}{\sqrt{\alpha}}\right)^2}\D x\\
  &= b\ln\left(1+\frac{\alpha}{b^2}\right) - \ln\left(1+\alpha\right) +2 \left[ \sqrt{\alpha}\arctan\left(\frac{x}{\sqrt{\alpha}}\right) \right]_1^b \\
  &= b\ln\left(1+\frac{\alpha}{b^2}\right) - \ln\left(1+\alpha\right) + 2 \sqrt{\alpha}\arctan\left(\frac{b}{\sqrt{\alpha}}\right) - 2 \sqrt{\alpha}\arctan\left(\frac{1}{\sqrt{\alpha}}\right).
\end{align*}
Or $b\ln\left(1+\frac{\alpha}{b^2}\right) \eq{+\infty}b\frac{\alpha}{b^2}=\frac{\alpha}{b}\tendversen{b\to +\infty} 0$, et $\arctan\left(\frac{b}{\sqrt{\alpha}}\right) \tendversen{b\to +\infty} \frac{\pi}{2}$. Ainsi, par somme de limites :
\[ \boxed{\int_1^{+\infty} \ln\left(1+\frac{\alpha}{x^2}\right) \D x = -\ln(1+\alpha)+\pi\sqrt{\alpha}-2\sqrt{\alpha}\arctan\left(\frac{1}{\sqrt{\alpha}}\right).} \]
  \item $x\donne \frac{\arctan(x)}{x^2}$ est continue sur $\interfo{1 +\infty}$. En $+\infty$, \[ \frac{\arctan(x)}{x^2}\eq{+\infty} \frac{\pi}{2x^2}. \]
  Pusique $\ds{\int_1^{+\infty} \frac{\pi}{2x^2}\D x}$ converge (Riemann avec $2>1$). Par comparaison de fonctions positives, l'intégrale $\ds{\int_1^{+\infty} \frac{\arctan(x)}{x^2}\D x}$ converge. \\Faisons une intégration par parties. Soit $b>1$. On pose $u:x\donne \arctan(x)$ et $v':x\donne \frac{1}{x^2}$. Ainsi, $u':x\donne \frac{1}{1+x^2}$ et $v:x\donne -\frac{1}{x}$. Les fonctions $u$ et $v$ sont de classe $\CC^1$ sur $\interfo{1 +\infty}$. Par IPP :
  \begin{align*}
    \int_1^b \frac{\arctan(x)}{x^2}\D x &= \left[ -\frac{1}{x}\arctan(x) \right]_1^b - \int_1^b -\frac{1}{x}\frac{1}{1+x^2}\D x\\
    &= \arctan(1)-\frac{\arctan(b)}{b} + \int_1^b \frac{1+x^2-x^2}{x(1+x^2)} \D x\\
    &= \frac{\pi}{4} -\frac{\arctan(b)}{b} + \int_1^b \frac{1}{x} - \frac{x}{1+x^2} \D x\\
    &= \frac{\pi}{4} -\frac{\arctan(b)}{b} +  \left[\ln(|x|)-\frac{1}{2}\ln(|1+x^2|) \right]_1^b\\
    &= \frac{\pi}{4} -\frac{\arctan(b)}{b} +  \ln\left(\frac{b}{\sqrt{1+b^2}}\right) +\frac12\ln(2)
  \end{align*}
  Or, par quotient, $\frac{\arctan(b)}{b}\tendversen{b\to +\infty} 0$ et $\ds{\frac{b}{\sqrt{1+b^2}}\eq{+\infty} 1}$ donc par composée, $\ds{\ln\left(\frac{b}{\sqrt{1+b^2}}\right)\tendversen{b\to +\infty} 0}$. Finalement,
  \[ \boxed{\int_1^{+\infty} \frac{\arctan(x)}{x^2}\D x = \frac{\pi}{4}+\frac{1}{2}\ln(2).}\]
  \item $x\donne \frac{1}{\eu{x}+\eu{-x}}$ est continue sur $\R$. Remarquons que
  \[ \frac{1}{\eu{x}+\eu{-x}}\eq{+\infty} \frac{1}{\eu{x}}=\eu{-x} \qeq \frac{1}{\eu{x}+\eu{-x}} \eq{-\infty} \frac{1}{\eu{-x}}=\eu{x}. \]
  Puisque $\ds{\int_0^{+\infty} \eu{-x}\D x}$ et $\ds{\int_{-\infty}^0 \eu{x}\D x}$ convergent, par comparaison de fonctions positives, notre intégrale converge.

$x: u \donne \ln(u)$ est de classe $\CC^1$ strictement croissante. On a \[ \lim_{x\to 0^+} \ln(x) = -\infty,\quad \lim_{x\to +\infty} \ln(x)=+\infty \qeq \D x = \frac{\D u}{u}.\]
Par changement de variable,
\begin{align*}
  \int_{-\infty}^{+\infty} \frac{\D x}{\eu{x}+\eu{-x}} &= \int_0^{+\infty} \frac{1}{\eu{\ln(u)}+\eu{-\ln(u)}} \frac{\D u}{u}\\
   &= \int_0^{+\infty} \frac{1}{u+\frac{1}{u}}\frac{\D u}{u} \\
   &= \int_0^{+\infty} \frac{1}{u^2+1}\D u = \left[ \arctan(u)\right]_0^{+\infty} =\frac{\pi}{2}.
\end{align*}
Ainsi, \[ \boxed{\int_{-\infty}^{+\infty} \frac{\D x}{\eu{x}+\eu{-x}} = \frac{\pi}{2}.}\]
  \item La fonction $f:x\donne \frac{x\ln(x)}{(1+x^2)^2}$ est continue sur $\R>0$. Remarquons que, par conséquence des croissances comparées, \[ \lim_{x\to 0} \frac{x\ln(x)}{(1+x^2)^2}= 0. \]
  Ainsi, $f$ est prolongeable par continuité en $0$, et l'intégrale est donc faussement impropre en $0$.

  En $+\infty$ :
  \[ x^2f(x) = \frac{x^3\ln(x)}{(1+x^2)^2}\eq{+\infty} \frac{x^3\ln(x)}{x^3}=\frac{\ln(x)}{x}\tendversen{x\to +\infty} 0. \]
  Ainsi, $\ds{\frac{x\ln(x)}{(1+x^2)^2} = \petito[+\infty]{\frac{1}{x^2}}}$. Puisque $\ds{\int_1^{+\infty} \frac{\D x}{x^2}}$ converge, par comparaison de fonctions positives, l'intégrale de $\ds{\int_1^{+\infty} f(t)\D t}$ converge. Ainsi, notre intégrale converge.

  $x=\frac1t$ est de classe $\CC^1$ strictement décroissante sur $\R>$. On a
  \[ \lim_{t\to 0^+}\frac{1}{t}=+\infty,\quad \lim_{t\to +\infty} \frac{1}{t}=0 \qeq \D x = -\frac{1}{t^2}\D t.\]
  Par changement de variable :
  \begin{align*}
    \int_0^{+\infty} \frac{x\ln(x)}{(1+x^2)^2} &= \int_{+\infty}^{0} \frac{\frac{1}{t}\ln\left(\frac{1}{t}\right)}{\left(1+\frac{1}{t^2}\right)^2}\times -\frac{\D t}{t^2}\\
    &= \int_0^{+\infty} \frac{-\ln(t)t^4}{(t^2+1)^2 t^3}\D t = -\int_0^{+\infty} \frac{t\ln(t)}{(1+t^2)^2}\D t.
  \end{align*}
  Ainsi, \[ \boxed{\int_0^{+\infty} \frac{x\ln(x)}{(1+x^2)^2}\D x=0}.\]
  \item $x\donne \frac{1}{(1+x^2)^n}$ est continue sur $\R+$. On a \[ \frac{1}{(1+x^2)^n} \eq{+\infty} \frac{1}{x^{2n}}. \]
  Puisque $n\geq 1$, $2n\geq 2 >1$, et donc $\ds{\int_1^{+\infty} \frac{\D t}{t^{2n}}}$ converge. Par comparaison de fonctions positives, l'intégrale $\ds{\int_1^{+\infty} \frac{\D x}{(1+x^2)^n}}$ converge, et donc $I_n$ converge pour tout $n\geq 1$.

  Soit $b>0$. Remarquons que :
  \begin{align*}
    \int_0^b \frac{\D x}{(1+x^2)^{n+1}} &= \int_0^b \frac{1+x^2-x^2}{(1+x^2)^{n+1}}\D x\\
    &= \int_0^b \frac{\D x}{(1+x^2)^n} - \int_0^b \frac{x^2}{(1+x^2)^{n+1}}\D x
  \end{align*}
  Procédons par intégration par parties dans la deuxième intégrale. On pose $u:x\donne x$, et $v':x\donne \frac{x}{(1+x^2)^{n+1}}$, c'est-à-dire $u':x\donne 1$ et $v:x\donne -\frac{1}{2n}\frac{1}{(1+x^2)^n}$. $u$ et $v$ sont de classe $\CC^1$ sur $\interff{0 b}$ et on a :
  \begin{align*}
    \int_0^b \frac{x^2}{(1+x^2)^{n+1}}\D x &= \left[ -x\frac{1}{2n}\frac{1}{(1+x^2)^n} \right]_0^b - \int_0^b -\frac{1}{2n}\frac{1}{(1+x^2)^n}\D x.
  \end{align*}
  Par convergence de $I_n$, on constate alors que
  \[ \int_0^b \frac{x^2}{(1+x^2)^{n+1}}\D x \tendversen{b\to +\infty} \frac{1}{2n}I_n \]
  et finalement
  \[ \int_0^b \frac{\D x}{(1+x^2)^{n+1}} \tendversen{b\to +\infty} I_n - \frac{1}{2n}I_n\]
  ce qui nous donne
  \[ \boxed{\forall n\in \N*,\quad I_{n+1}=\frac{2n-1}{2n} I_n.}\]
  Puisque $I_1=\ds{\int_0^{+\infty} \frac{1}{1+x^2}=\frac{\pi}{2}}$, par récurrence rapide, on montre que
  \[ \boxed{\forall n\in \N*,\quad I_n = \frac{(2n-2)!}{2^{2n-2} (n-1)!^2}\frac{\pi}{2}} \]
\end{enumerate}
}
\end{correction}
%%% Fin exercice %%%
