%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{25}{2}[Prolongement et dérivées]
Pour chacune des fonctions suivantes, donner le domaine de définition, et les prolonger éventuellement par continuité. Préciser ensuite les intervalles sur lesquels elles sont dérivables, et calculer leurs dérivées (à droite ou à gauche si nécessaire). Sont-elles de classe $\CC^1$ ?
\begin{multicols}{3}
\begin{enumerate}
  \itemsep.5em
  \item $x\donne \dfrac{x}{1+|x|}$
  \item $x\mapsto x\ln(x)-x$,
  \item $x\mapsto x^{1/\ln(x)}$,
  \item $x\mapsto x^x$,
  \item $x\donne \sqrt{x^3-x^2}$,
  \item $x\mapsto 1-\cos\left(\sqrt{|x-1|}\right)$.
\end{enumerate}
\end{multicols}
\end{exoApp}

\begin{correction}
\begin{enumerate}
  \item La fonction $f:x\mapsto \frac{x}{1+|x|}$ est définie et continue sur $\R$, comme quotient de fonctions continues dont le dénominateur ne s'annule pas. Elle est dérivable sur $\R<$ et sur $\R>$ :
  \begin{itemize}
    \item Sur $\R>$, $x\mapsto \frac{x}{1+|x|}=\frac{x}{1+x}$ est dérivable et a pour dérivée $x\mapsto \frac{1}{(1+x)^2}$.
    \item Sur $\R<$, $x\mapsto \frac{x}{1+|x|}=\frac{x}{1-x}$ est dérivable et a pour dérivée $x\mapsto \frac{1}{(1-x)^2}$.
  \end{itemize}
  On constate que \[ \lim_{x\to O^+} \frac{1}{(1+x)^2} = \lim_{x\to 0^-} \frac{1}{(1-x)^2}  = 1. \]
  D'après le théorème de prolongement $\CC^1$, la fonction est de classe $\CC^1$ sur $\R$, et $f'(0)=1$.
  \item La fonction $f:x\mapsto x\ln(x)-x$ est définie, continue et dérivable sur $\R>$ par somme et quotient, et sa dérivée est $f':x\mapsto \ln(x)$.

  On constate de plus que \[ \lim_{x\to 0^+} f(x) = 0 \text{ par croissance comparée.}\]
  Ainsi, la fonction $f$ est continue (ou prolongeable par continuité) sur $\R+$, en posant $f(0)=0$.

  Calculons le taux d'accroissement en $0$ :
  \[ \frac{f(x)-f(0)}{x} = \frac{x\ln(x)-x}{x}=\frac{\ln(x)-1}{x}\tendversen{x\to 0^+} -\infty. \]
  La fonction n'est donc pas dérivable en $0$.\\\textbf{Bilan} : $f$ est continue sur $\R+$, et dérivable et classe $\CC^1$ sur $\R>$.
  \item On constate que $f:x\mapsto x^{\frac{1}{\ln(x)}}$ est définie, continue et dérivable sur $\R>$ par composée. Mais on constate que
  \[ \forall x\in \R>,\quad x^{\frac{1}{\ln(x)}} = \eu{ \frac{1}{\ln(x)}\ln(x)} = \eu{1}. \]
  Ainsi, on peut prolonger $f$ sur $\R$ tout entier, et elle y est de classe $\CC^1$, de dérivée nulle.
  \item De même, $f:x\mapsto x^x$ est définie, continue, dérivable et de classe $\CC^1$ sur $\R>$ par composée. En écrivant \[ f:x\mapsto \eu{x\ln(x)} \]
  sa dérivée s'écrit \[ f':x\mapsto (\ln(x)+1)x^x. \]
  De plus, puisque $x\ln(x)\tendversen{x\to 0^+} 0$, par composée $f(x)\tendversen{x\to 0^+} 1$. On peut prolonger par continuité $f$ en $0^+$, en posant $f(0)=1$.

  Déterminons le taux d'accroissement :
  \begin{align*}
    \frac{x^x - 1}{x} &= \frac{\eu{x\ln(x)}-1}{x} \\
    &= \frac{\eu{x\ln(x)}-\eu{0}}{x\ln(x)}\ln(x)
  \end{align*}
  Par composée, puisque $x\ln(x)\tendversen{x\to 0} 0$, alors  \[ \frac{\eu{x\ln(x)}-\eu{0}}{x\ln(x)}\tendversen{x\to 0} \exp'(0)=1 \]
  et donc par produit \[ \lim_{x\to 0} \frac{f(x)-f(0)}{x}=-\infty. \]
  La fonction $f$ n'est donc pas dérivable en $0$.\\\textbf{Bilan} : $f$ est continue sur $\R+$, dérivable et de classe $\CC^1$ sur $\R>$.
  \item Remarquons que $f:x\mapsto \sqrt{x^3-x^2}$ est définie et continue si et seulement si $x^3-x^2\geq 0$, c'est-à-dire $x^2(x-1)\geq 0$. En dressant le tableau de signes :
\begin{center}
  \begin{tikzpicture}
     \tkzTabInit{$x$ / 1 , $x^3-x^2$ / 1}{$-\infty$, $0$, $1$, $+\infty$}
     \tkzTabLine{, - , z, -, z, +, }
  \end{tikzpicture}
\end{center}
  Ainsi, $f$ est définie et continue sur $\interfo{1 +\infty}$. De même, elle est dérivable sur $\interoo{1 +\infty}$, et sa dérivée est \[ x\mapsto (3x^2-2x)\frac{1}{2\sqrt{x^3-x^2}} \]
  Déterminons la dérivabilité potentielle de $f$ en $1$ :
  \[ \frac{f(x)-f(1)}{x-1} = \frac{\sqrt{x^3-x^2}}{x-1} = \sqrt{\frac{x^2}{x-1}}\tendversen{x\to 1} +\infty. \]
  Ainsi, $f$ n'est pas dérivable en $1$.\\\textbf{Bilan} : $f$ est définie et continue sur $\interfo{1 +\infty}$, dérivable et de classe $\CC^1$ sur $\interoo{1 +\infty}$.
  \item Notons $f:x\mapsto 1-\cos\left(\sqrt{|x-1|}\right)$. $f$ est définie et continue sur $\R$ par composée. De plus, elle est dérivable, par composée, sur $\R\setminus \{1\}$, et :
  \begin{itemize}
    \item Sur $\interoo{1 +\infty}$, $f:x\mapsto 1-\cos(\sqrt{x-1})$ et donc \[ f':x\mapsto \frac{1}{2\sqrt{x-1}}\sin(\sqrt{x-1}).\]
    \item Sur $\interoo{-\infty{} 1}$, $f:x\mapsto 1-\cos(\sqrt{1-x})$ et donc \[ f':x\mapsto \frac{-1}{2\sqrt{1-x}}\sin(\sqrt{1-x}).\]
  \end{itemize}
  Intéressons-nous à la dérivabilité à droite et à gauche en $1$:
  \begin{itemize}
    \item En $1^+$ :
\begin{align*}
  \frac{f(x)-f(1)}{x-1} &= \frac{1-\cos(\sqrt{x-1})}{x-1} \\
  &= \frac{(1-\cos(\sqrt{x-1}))(1+\cos(\sqrt{x-1}))}{(x-1)(1+\cos(\sqrt{x-1}))}\\
  &= \frac{1-\cos^2(\sqrt{x-1})}{(x-1)(1+\cos(\sqrt{x-1}))}\\
  &= \frac{\sin^2(\sqrt{x-1})}{x-1} \frac{1}{1+\cos(\sqrt{x-1})}\\
  &= \left( \frac{\sin(\sqrt{x-1})}{\sqrt{x-1}}\right)^2 \frac{1}{1+\cos(\sqrt{x-1})}
  \end{align*}
  Or $\sqrt{x-1}\tendversen{x\to 1} 0$. Par composée et dérivabilité de $\sin$ en $0$ :
  \[ \lim_{x\to 1} \frac{\sin(\sqrt{x-1})}{\sqrt{x-1}}=\sin'(0) = 1 \]
  soit, par composée et quotient
  \[ \lim_{x\to 1^+} \frac{f(x)-f(1)}{x-1} = \frac{1}{2} \]
  \item Par le même raisonnement
  \[ \lim_{x\to 1^-} \frac{f(x)-f(1)}{x-1} = -\frac{1}{2} \]
  \end{itemize}
  Ainsi, $f$ n'est pas dérivable en $1$, mais admet des demi-tangentes à  gauche et à droite, avec $f'_d(1)=\frac12$ et $f'_g(1)=-\frac12$.\\\textbf{Bilan} : $f$ est continue sur $\R$, dérivable et $\CC^1$ sur $\R \setminus\{1\}$, et admet en $1$ des dérivées à droite et à gauche.
\end{enumerate}
\end{correction}
%%% Fin exercice %%%
