%!TeX root=../../../encours.nouveau.tex
%%% Début exercice %%%

\begin{exoApp}{25}{2}[Suite d'intégrales I]
Pour tout entier $n$, on pose $$u_n=\int_0^{1/2} \frac{x^n}{1-x^2}\dd x$$
\begin{enumerate}
    \item Déterminer deux réels $a$ et $b$ tels que pour tout $x\in \R \backslash \{-1;1\}$, $\frac{1}{1-x^2}=\frac{a}{1-x}+\frac{b}{1+x}$. En déduire la valeur de $u_0$.
    \item Calculer $u_1$.
    \item Montrer que pour tout entier $n$, $$u_n-u_{n+2}=\frac{1}{(n+1)2^{n+1}}$$
    En déduire $u_2$ et $u_3$.
    \item Montrer que la suite $(u_n)$ est décroissante, et minorée par $0$.
    \item En minorant $1-x^2$, montrer que pour tout entier $n$, $\displaystyle{u_n\leq \frac{4}{3(n+1)2^{n+1}}}$. En déduire la limite de la suite $u$.
\end{enumerate}

\end{exoApp}

\begin{correction}
	Remarquons déjà que, pour tout entier $n$, la fonction $x\mapsto \frac{x^n}{1-x^2}$ est définie et continue sur $\left[0;\frac{1}{2}\right]$. Toutes les intégrales considérées existent donc bien. \begin{enumerate}
	\item En mettant au même dénominateur,
			$$\forall~x\in \R \backslash \{-1;1\},~~\frac{1}{1-x^2}=\frac{a}{1-x}+\frac{b}{1+x}=\frac{(a-b)x+(a+b)}{1-x^2}$$
			Par identification des coefficients, $$\left \{ \begin{array}{ccc}
				a-b&=&0\\a+b&=&1
			\end{array}\right. \Leftrightarrow a=b=\frac{1}{2}$$
			Ainsi, pour tout $x$ différent de $-1$ et $1$, $$\frac{1}{1-x^2}=\frac{1/2}{1-x}+\frac{1/2}{1+x}$$
			et donc
			$$u_0=\int_0^{1/2} \frac{1}{1-x^2}\dd x=\int_0^{1/2} \frac{1/2}{1-x}+\frac{1/2}{1+x} \dd x = \left [ -\frac{1}{2}\ln|1-x|+\frac{1}{2}\ln|1+x|\right]_0^{1/2}$$
			soit
			$$\boxed{u_0=-\frac{1}{2}\ln\left(\frac{1}{2}\right)+\frac{1}{2}\ln \left(\frac{3}{2}\right) = \ln \left (\sqrt{3}\right)}$$
	\item $$u_1=\int_0^{1/2} \frac{x}{1-x^2}\dd x = -\frac{1}{2}\int_0^{1/2} \frac{-2x}{1-x^2}\dd x=-\frac{1}{2}\left[ \ln(1-x^2)\right]_0^{1/2}$$
			et donc
			$$\boxed{u_1=-\frac{1}{2}\ln \left(\frac{3}{4}\right) = \ln \left (\sqrt{ \frac{4}{3}}\right)}$$
	\item Par linéarité de l'intégrale, $$u_n-u_{n+2}=\int_0^{1/2} \frac{x^n}{1-x^2}\dd x - \int_0^{1/2} \frac{x^{n+2}}{1-x^2}\dd x = \int_0^{1/2} \frac{x^n-x^{n+2}}{1-x^2}\dd x$$
			soit
			$$u_n-u_{n+2}=\int_0^{1/2} \frac{x^n(1-x^2)}{1-x^2}\dd x=\int_0^{1/2} x^n\dd x = \left[ \frac{x^{n+1}}{n+1} \right]_0^{1/2}$$
			et donc
			$$\boxed{u_n-u_{n+2} = \frac{\left(1/2\right)^{n+1}}{n+1}=\frac{1}{2^{n+1}(n+1)}}$$
		Pour $n=0$, on obtient donc $u_0-u_2=\frac{1}{2}$, soit $$u_2=u_0-\frac{1}{2}=\ln \left(\sqrt{3}\right) - \frac{1}{2}$$
		et pour $n=1$, il vient $u_1-u_3=\frac{1}{2^2\times 2}$, soit $$u_3=u_1-\frac{1}{8}=\ln \left (\sqrt{\frac{4}{3}}\right)-\frac{1}{8}$$
	\item Pour déterminer la monotonie de la suite $u$, déterminons pour tout entier $n$ le signe de $u_{n+1}-u_n$ :
			$$u_{n+1}-u_n=\int_0^{1/2} \frac{x^{n+1}}{1-x^2}\dd x-\int_0^{1/2} \frac{x^n}{1-x^2}\dd x = \int_0^{1/2} \frac{x^{n+1}-x^n}{1-x^2}\dd x \textrm{ par linéarité}$$
			donc
			$$u_{n+1}-u_n=\int_0^{1/2} \frac{x^n(x-1)}{1-x^2}\dd x$$
			Or, sur $\left[0; \frac{1}{2}\right]$, $x^n\geq 0$, $x-1\leq 0$ et $1-x^2 >0$. Ainsi, sur cet intervalle, $\displaystyle{\frac{x^n(x-1)}{1-x^2}} \leq 0$. Puisque $0<\frac{1}{2}$, par positivité de l'intégrale, on en déduit donc que $\displaystyle{\int_0^{1/2} \frac{x^n(x-1)}{1-x^2}\dd x\leq 0}$.
			\\\textbf{Bilan} : pour tout entier naturel $n$, $u_{n+1}-u_n\leq 0$ : la suite $(u_n)$ est décroissante.\\
			De plus, toujours sur $\left[0; \frac{1}{2}\right]$, $x^n\geq 0$ et $1-x^2\geq 0$. Par positivité de l'intégrale, on a donc $\displaystyle{\int_0^{1/2} \frac{x^n}{1-x^2}\dd x\geq 0}$ : la suite $(u_n)$ est bien positive.
	\item Remarquons déjà qu'étant décroissante et minorée, par théorème de convergence monotone, la suite $(u_n)$ converge.
		\begin{methode}
			Pour encadrer une intégrale, on encadre ce qui est dans l'intégrale, et on utilise la croissance de l'intégrale.
		\end{methode}
		Pour tout $x\in \left[0;\frac{1}{2}\right]$; $0 \leq x \leq \frac{1}{2} \Rightarrow 0 \leq x^2 \leq \frac{1}{4}$ par croissance de la fonction carrée sur $\R^+$.
		Soit
		$$0\leq x \leq \frac{1}{2} \Leftrightarrow 1-x^2 \geq \frac{3}{4}$$
		Donc sur $\left[0;\frac{1}{2}\right]$, on a
		$$\frac{1}{1-x^2} \leq \frac{4}{3}$$
		ainsi
		$$\frac{x^n}{1-x^2} \leq \frac{4}{3} x^n \textrm{  car } x^n \geq 0$$
		Par croissance de l'intégrale ($0\leq \frac{1}{3}$), on a alors
		$$\int_0^{1/2} \frac{x^n}{1-x^2}\dd x \leq \int_0^{1/2} \frac{4}{3}x^n\dd x$$
		et donc
		$$u_n \leq \left[ \frac{4}{3} \frac{x^{n+1}}{n+1} \right] = \frac{4}{3} \frac{\left(1/2\right)^{n+1}}{n+1}=\frac{4}{3(n+1)2^{n+1}}$$
		Par produit, et puisque $2>1$, on a
		$$\lim_{n\rightarrow +\infty} (n+1)2^{n+1} =+\infty$$
		et par quotient
		$$\lim_{n\rightarrow +\infty} \frac{4}{3(n+1)2^{n+1}}=0$$
		Or, pour tout entier $n$, nous avons $\displaystyle{0\leq u_n \leq \frac{4}{3(n+1)2^{n+1}}}$. Ainsi, par le théorème d'encadrement, la limite de $(u_n)$ existe et vaut $0$:
		$$\boxed{\lim_{n\rightarrow +\infty} u_n=0}$$

\end{enumerate}

\end{correction}
%%% Fin exercice %%%
